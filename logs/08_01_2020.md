# 08 Gennaio 2020 (15:00 - 17:00)

#### Membri presenti:

1. Alessandro Frenna `scrum master`
2. Manuel Ciancimino
3. Marco Ingrande
4. Andrea Muscarella

#### Argomenti all'ordine del giorno

1. Fine lavori secondo sprint
2. Sesto Scrum - Fine secondo sprint

---

#### Fine lavori secondo sprint

Oggi abbiamo concluso le ultime storie utente del secondo sprint in dipartimento rifinendo i test e creando le pagine html relative alla Client Platform.

#### Sesto Scrum - Fine secondo sprint

Finita la parte di sviluppo abbiamo iniziato lo scrum finale per verificare il funzionalità della Client Platform sviluppate e concluse. Abbiamo verificato che i test funzionassero come previsto e abbiamo verificato che tutto funzionasse.

Alla fine della verifica lo scrum si è concluso.
