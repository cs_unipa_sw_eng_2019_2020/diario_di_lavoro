# Ingegneria del software - A.A 2019/2020

## Diario di bordo del progetto

---

### Membri del team:

1. Alessandro Frenna `scrum master`
2. Andrea Muscarella
3. Manuel Ciancimino
4. Marco Ingrande

---

### Resoconto degli incontri

- [`16 e 18 Ottobre 2019`](./logs/16&18_10_2019.md)
- [`06 Novembre 2019`](./logs/06_11_2019.md)
- [`08 Novembre 2019`](./logs/08_11_2019.md)
- [`20 e 22 Novembre 2019`](./logs/20&22_11_2019.md)
- [`04 Dicembre 2019`](./logs/06_12_2019.md)
- [`11 Dicembre 2019`](./logs/11_12_2019.md)
- [`13 Dicembre 2019`](./logs/13_12_2019.md)
- [`18 Dicembre 2019`](./logs/18_12_2019.md)
- [`20 Dicembre 2019`](./logs/20_12_2019.md)
- [`27 Dicembre 2019`](./logs/27_12_2019.md)
- [`30 Dicembre 2019`](./logs/30_12_2019.md)
- [`02 Gennaio 2020`](./logs/02_01_2020.md)
- [`05 Gennaio 2020`](./logs/05_01_2020.md)
- [`07 Gennaio 2020`](./logs/07_01_2020.md)
- [`08 Gennaio 2020`](./logs/08_01_2020.md)
- [`09 Gennaio 2020`](./logs/09_01_2020.md)
